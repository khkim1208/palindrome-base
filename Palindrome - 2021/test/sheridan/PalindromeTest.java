package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PalindromeTest {

	@Test
	public void testIsPalindromeRegular( ) {
		boolean test = Palindrome.isPalindrome("racecar");
		assertTrue("Invalid value", test);
	}

	@Test
	public void testIsPalindromeException( ) {
		boolean test = Palindrome.isPalindrome("thisfails");
		assertFalse("Invalid value", test);
	}
	@Test
	public void testIsPalindromeBoundaryIn( ) {
		boolean test = Palindrome.isPalindrome("Taco cat");
		assertTrue("Invalid value", test);
	}
	@Test
	public void testIsPalindromeBoundaryOut( ) {
		boolean test = Palindrome.isPalindrome("race on car");
		assertFalse("Invalid value", test);
	}	
	
}
